# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: py3.6
#     language: python
#     name: python3
# ---

# +
import itertools
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
import seaborn as sb
from math import floor, log10, copysign

pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')


def round_to_n(x, n):
    sign = lambda x: copysign(1, x)
    #Round x to n significant figures
    return round(x, -int(floor(sign(x) * log10(abs(x)))) + n)

def str_fmt(x, n=2):
    #Format x into nice Latex rounding to n
    power = int(log10(round_to_n(x, 0)))
    f_SF = round_to_n(x, n) * pow(10, -power)
    return u"{:.3f} \u00D7 10^{}".format(f_SF, power)

# matrix exponential
def eig_expm(A):
        d, Y = np.linalg.eig(A)
        Yinv = np.linalg.pinv(Y)
        D = np.diag(np.exp(d))
        Y = np.asmatrix(Y)
        D = np.asmatrix(D)
        Yinv = np.asmatrix(Yinv)
        B = Y*D*Yinv
        return B
#IRM to Q
def get_Pt(filename):
    df = pd.read_csv(filename, sep="\t", header=None, index_col=0, skiprows=1)
    df.columns = df.index.tolist()
    nstates = len(df.index.tolist())
    arr = df.values
    for i in range(0,nstates):
        arr[i,i] = (np.sum(arr[i,:]) - arr[i,i])*-1
    Pt_df = pd.DataFrame(eig_expm(arr), index=df.index, columns=df.columns)
    return Pt_df

RAM55_Pt_df = get_Pt("data/rotamers_adj_shared_count_matrix.merged.Dayhoff_norm.IRM.superQ-star")
RUM20_Pt_df = get_Pt("data/restype_adj_shared_count_matrix.merged.IRM.Q-star.neg_diag")

# +
# compute transition probabilities in AA and rotamer sapce
# for all possible 2-step stransitions betweeen 3 distinct AA

#load exchangeabilities
exc_20_filename = "data/restype_adj_shared_count_matrix.merged.exchangeability.Q-star"
exc_55_filename = "data/rotamers_adj_shared_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star"
RAM55_exc_df =  pd.read_csv(exc_55_filename,
                            sep="\t", header=0, index_col=0)
RUM20_exc_df =  pd.read_csv(exc_20_filename,
                            sep="\t", header=0, index_col=0)

#all possible 2-step transitions (without replacement) in AA space
transitions_arr = []
for AA_transitions in list(itertools.combinations(RUM20_Pt_df.columns, 3)):
    s1,s2,s3 = AA_transitions
    ex20_arr = [RUM20_exc_df[s1][s2], RUM20_Pt_df[s2][s3]]
    p_20 = RUM20_Pt_df[s1][s2] *  RUM20_Pt_df[s2][s3]
    ROTA_transitions = [[r for r in RAM55_Pt_df.columns if r.startswith(aa)] for aa in AA_transitions]
    out_arr = []
    
    #the corresponding best and worst ROTA transitions in Rotamer space
    for trio in list(itertools.product(*ROTA_transitions, repeat=1)):
        s1, s2, s3 = trio
        ex55_arr = [RAM55_exc_df[s1][s2], RAM55_exc_df[s2][s3]]
        p = RAM55_Pt_df[s1][s2] *  RAM55_Pt_df[s2][s3]
        out_arr.append(["{} -> {} -> {}".format(*trio), p, ex55_arr])
    out_df = pd.DataFrame(out_arr, columns=["route", "p", "ex55_arr"])
    out_df = out_df.sort_values(by=["p"], ascending=False)
    best_t, best_p, best_ex55_arr = out_df.values[0]
    worst_t, worst_p, worst_ex55_arr = out_df.values[-1]
    d =  best_p - worst_p
    transitions_arr.append(["{} -> {} -> {}".format(*AA_transitions),
                            ex20_arr, p_20,
                            best_t, best_p, worst_t, worst_p, d, best_ex55_arr, worst_ex55_arr])
#sort by delta p, print top 20
transitions_df = pd.DataFrame(transitions_arr, columns=["AA_route", "exc_20_arr", 
                                                        "p_20", "best_ROTA_route", "best_ROTA_p", 
                                                        "worst_ROTA_route", "worst_ROTA_p", "d",
                                                        "best_exc55_arr", "worst_exc55_arr"])
transitions_df = transitions_df.sort_values(by="d", ascending=False)
transitions_df.head(10)
#write_df = transitions_df.head(20).to_csv("RUM20_v_RAM55_trajectory.tsv", sep="\t")


# +
# represent transition probabilities as heatmaps 
# for the top 10 most diverging (AA P vs. rota P) transitions

cmap = sb.cubehelix_palette(start=.5, rot=-.7, light=.97, as_cmap=True)

in_df = transitions_df.head(10)[["AA_route", "best_ROTA_route", 
                                 "worst_ROTA_route", "best_ROTA_p", "worst_ROTA_p"]]


MAX = RAM55_Pt_df.max().max()
for row  in in_df.values:
    AA_route, best_ROTA_route, worst_ROTA_route, best_ROTA_p, worst_ROTA_p = row
    id_str = "_".join(AA_route.split(" -> "))
    A1, A2, A3 = AA_route.split(" -> ")
    b1, b2, b3 = best_ROTA_route.split(" -> ")
    w1, w2, w3 = worst_ROTA_route.split(" -> ")
    submatrix1 = RAM55_Pt_df[[s for s in RAM55_Pt_df.columns if s.startswith(A1)]].loc[[s for s in RAM55_Pt_df.columns if s.startswith(A2)]]
    submatrix2 = RAM55_Pt_df[[s for s in RAM55_Pt_df.columns if s.startswith(A2)]].loc[[s for s in RAM55_Pt_df.columns if s.startswith(A3)]]
    
    fig, axes = plt.subplots(1,2)
    
    
    for ax, arr, best_cell, worst_cell in zip(axes, [submatrix1, submatrix2], [(b1, b2), (b2, b3)], [(w1, w2), (w2, w3)]):
        #sb.heatmap(arr.values, vmax=MAX, square=True, annot=True, cmap=cmap, ax = ax)
        ax.matshow(arr.values, interpolation='nearest',
                   cmap=cmap, norm=LogNorm(vmin=0.01, vmax=MAX),
                   alpha=1, aspect=1)
        ax.tick_params(axis='both', length=3, rotation=30, labelsize=12)
        ax.set_yticks(np.arange(len(arr.index.values)))
        ax.set_yticklabels(arr.index.values)
        ax.set_xticks(np.arange(len(arr.columns)))
        ax.set_xticklabels(arr.columns)
        
        # annotate with array values
        for i in range(len(arr.index.values)):
            for j in range(len(arr.columns)):
                text = ax.text(j, i, "%.4f" % arr.values[i, j],
                               ha="center", va="center", color="k")
        
        
        # annotate the best and worst trajectories
        x_coords = dict(list(zip(arr.columns, [x - .5 for x in ax.get_xticks()])))
        y_coords = dict(list(zip(arr.index.values, [y - .5 for y in ax.get_yticks()])))
        best_idx = (x_coords[best_cell[0]], y_coords[best_cell[1]])
        worst_idx = (x_coords[worst_cell[0]], y_coords[worst_cell[1]])
        ax.add_patch(Rectangle(best_idx, 1, 1, fill=False, edgecolor='blue', lw=1.5, zorder=20))
        ax.add_patch(Rectangle(worst_idx, 1, 1, fill=False, edgecolor='red', lw=1.5, zorder=20))                   
    
    best_txt = "P(" + " → ".join(best_ROTA_route.split(" -> ")) + ") = " + str_fmt(best_ROTA_p)
    fig.text(.3,.14, best_txt, color="blue")
    
    worst_txt = "P(" + " → ".join(worst_ROTA_route.split(" -> ")) + ") = " + str_fmt(worst_ROTA_p)
    fig.text(.3,.1, worst_txt, color="red")
    fig.tight_layout()
    #fig.savefig("{}.heatmap.pdf".format(id_str), format='pdf', bbox_inches='tight', dpi=fig.dpi)
    #fig.savefig("{}.heatmap.svg".format(id_str), format='svg', bbox_inches='tight', dpi=fig.dpi)

# +
# Represent an example of a 3x3 exchangeability submatrix

cmap = sb.cubehelix_palette(start=.5, rot=-.7, light=.97, as_cmap=True)

exc_55_filename = "data/rotamers_adj_shared_count_matrix.merged.Dayhoff_norm.exchangeability.superQ-star"
RAM55_exc_df =  pd.read_csv(exc_55_filename,
                            sep="\t", header=0, index_col=0)
MAX=RAM55_exc_df.max().max()
arr = RAM55_exc_df[[s for s in RAM55_exc_df.columns if s.startswith("PHE")]].\
    loc[[s for s in RAM55_exc_df.columns if s.startswith("TRP")]]


fig, ax = plt.subplots(1,1, figsize=(5,5))

ax.matshow(arr.values, interpolation='nearest',
                   cmap=cmap, norm=LogNorm(vmin=0.01, vmax=MAX),
                   alpha=1, aspect=1)
ax.tick_params(axis='both', length=3, rotation=30, labelsize=12)
ax.set_yticks(np.arange(len(arr.index.values)))
ax.set_yticklabels(arr.index.values)
ax.set_xticks(np.arange(len(arr.columns)))
ax.set_xticklabels(arr.columns)

plt.show()
fig.savefig("PHE-TRP_exch.heatmap.pdf", format='pdf', bbox_inches='tight', dpi=fig.dpi)
