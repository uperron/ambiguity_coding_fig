# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import json
import requests
from pandas.io.json import json_normalize
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import ticker
import seaborn as sb
import warnings
warnings.filterwarnings('ignore')
pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
sb.set(font_scale=1.5)
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

colors = sb.cubehelix_palette(5, start=.5, rot=-.7, light=.97)

def url_response(url):
    r = requests.get(url=url)
    if r.status_code == 200:
        json_result = r.json()
        return json_result
    else:
        return None
    
# starting from a uniprot search output:
# https://www.uniprot.org/uniprot/?query=database:(type:pdb)%20database:(type:pfam%20pf00016)&fil=&sort=scoref = "uniprot-database%3A%28type%3Apdb%29+database%3A%28type%3Apfam+pf00016%29.tab"
# produces the input file for: https://bitbucket.org/uperron/rotamer_ambiguity

infile="data/uniprot-database%3A%28type%3Apdb%29+database%3A%28type%3Apfam+pf00016%29.tab"
uniprot_df = pd.read_csv(infile, sep="\t", header=0, index_col=None)
uniprot_df.head()
# -

df1 = uniprot_df.dropna(subset=["Cross-reference (PDB)"])
n = len(df1["Entry"].unique())
m = len(df1["Organism"].unique())
print("Dataset contains %d distinct Uniprot entries from %d unique species." % (n,m))

# expand on PDB_ids
df2 = df1.drop("Cross-reference (PDB)", axis=1)\
.join(df1["Cross-reference (PDB)"]\
      .str.split(pat=";", expand=True).stack()\
      .reset_index(drop=True, level=1).rename('PDB_id'))

# +
# select the best pdb for each species

frames = []
# For each PDB_id alignment get info about corresponding PDB entities
entry_search_url = 'http://www.ebi.ac.uk/pdbe/api/pdb/entry/molecules/'
for pdb_id in df2["PDB_id"]:
    search_url = entry_search_url + pdb_id.upper()
    r = url_response(search_url)
    if r != None:
        for molecule in r[pdb_id.lower()]:
            # filter for polypeptides and large subunits
            if  "polypeptide" in molecule.get('molecule_type') and molecule.get('length') > 400:
                df = json_normalize(molecule)
                df["PDB_id"] = [pdb_id] * len(df)
                frames.append(df)
mol_info_df = pd.concat(frames)
s = " 	PDB_id 	entity_id 	gene_name 	in_chains 	\
length 	molecule_name 	molecule_type 	mutation_flag 	pdb_sequence"
mol_info_df = mol_info_df[s.split()]
mol_info_df = pd.merge(df2, mol_info_df, on="PDB_id")

# discard mutated strctures
mol_info_df["mutation_flag"] = mol_info_df["mutation_flag"].fillna("NA")
mol_info_df = mol_info_df[mol_info_df["mutation_flag"] == "NA"] 

frames = []
# For each PDB_id in get experiment quality info about corresponding PDB entities
entry_search_url = 'https://www.ebi.ac.uk/pdbe/api/pdb/entry/experiment/'
for pdb_id, entity_id in mol_info_df[['PDB_id', 'entity_id']].values:
    entity_id = str(entity_id)
    search_url = entry_search_url + pdb_id.upper()
    r = url_response(search_url)
    if r != None:
        for data in r[pdb_id.lower()]:
            df = json_normalize(data)
            df["PDB_id"] = [pdb_id] * len(df)
            frames.append(df)
exp_info_df = pd.concat(frames)
exp_info_df = exp_info_df[['PDB_id', 'r_factor', 'resolution']]
mol_info_df = pd.merge(mol_info_df, exp_info_df, on="PDB_id")
mol_info_df = mol_info_df.sort_values(by=["Organism", "resolution"], ascending=True)
mol_info_df = mol_info_df.drop_duplicates(subset="Organism", keep='first')
# mol_info_df.to_csv("empirical_dataset_PFAM_PF00016_rubisco.txt", sep="\t", index=None)
mol_info_df.head()
# -

n = len(mol_info_df["Entry"].unique())
m = len(mol_info_df["Organism"].unique())
print("Dataset contains %d distinct Uniprot entries from %d unique species." % (n,m))
