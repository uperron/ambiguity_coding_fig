# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: py3.7
#     language: python
#     name: py3.7
# ---

# +
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import ticker
import seaborn as sb
import warnings
warnings.filterwarnings('ignore')
from scipy.stats import wilcoxon
from itertools import combinations
import warnings
warnings.filterwarnings('ignore')

sb.set(context='paper', style='darkgrid', rc={'figure.facecolor':'white'}, font_scale=1.2)

pd.options.display.float_format = '{:,.4f}'.format
pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
#sb.set(context='paper', style='darkgrid', rc={'figure.facecolor':'white'}, font_scale=1.2)
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
sb.set_style("ticks")
matplotlib.rc("font", serif='Helvetica Neue')
figsize = (7,3)

# homolog rotamer config reconstruction using RAM55 and rotastate assignment methods:

# NNC = nearest neighbour configuration
# the rotamer configuration is assigned based on the nearest rotasequence on the tree. 
# Where the AA is not conserved fall back to assigning based on the rotamer equilibrium
# equilibrium frequency in RAM55.

# MMC = masked nearest neighbour
# same as above only the nearest rotasequence is masked and the thus the next one over 
# is used to assign the rotamer configuration.

# based on uperron@ebi:/nfs/research1/goldman/umberto/bioinfotree/prj/rotamer_ambiguity/dataset/\
# rubisco_empirical/rubisco_empiricalx31x500_1-char.rotasequences.homology_term_rotaseq_ALL.accuracyvREF.tsv
# https://bitbucket.org/uperron/rotamer_ambiguity
accu_infile = "data/rubisco_empiricalx31x500_1-char.rotasequences.homology_\
term_rotaseq_ALL.accuracyvREF.tsv"
data = pd.read_csv(accu_infile, sep="\t", index_col=None)
# LG and RUM20 are the same (no AA reconstruction),
# drop LG and remove RUM20 from model name 
data = data[data["model"].str.contains('RUM|RAM', regex=True)]
data["model"].replace('RUM20_', '', inplace=True, regex=True)
data["model"].replace({"RAM55MNN" : "RAM55 MNN",
                      "relfreq" : "rel freq" }, inplace=True)
models = data.model.unique()
print(models)
data = data.sort_values(by=["taxon", "model"])
data.head(10)

# +
# The Wilcoxon signed-rank test tests the null hypothesis that two related paired samples come from 
# the same distribution. In particular, it tests whether the distribution of the differences x - y 
# is symmetric about zero. It is a non-parametric version of the paired T-test. A typical rule is 
# to require that n > 20 (here n=31).

arr = []
for m1, m2 in combinations(models, 2):
        X = data[data["model"] == m1]["ROTA_accuracy"].values
        Y = data[data["model"] == m2]["ROTA_accuracy"].values
        d = X - Y
        # Test the null hypothesis that there is no accuracy difference
        stat, pval = wilcoxon(d)
        # Test the null hypothesis that the median of the differences is positive
        # i.e. m1 > m2
        g_stat, g_pval = wilcoxon(d, alternative='greater')
        # Test the null hypothesis that the median of the differences is negative
        # i.e. m2 > m1
        l_stat, l_pval = wilcoxon(d, alternative='less')
        arr.append([m1, m2, stat, pval, g_stat, g_pval, l_stat, l_pval])
wil_df = pd.DataFrame(arr, columns=["model_1", "model_2", 
                                    "two-sided_wilcoxon_stat", "two-sided_Pval",
                                   "greater_wilcoxon_stat", "greater_Pval",
                                   "less_wilcoxon_stat", "less_Pval"])
wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]] = wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]].\
apply(lambda r : ['{:.6f}'.format(x) for x in r])
wil_df

# +
# same as above but looking at "ROTA_conserved_accu"

arr = []
for m1, m2 in combinations(models, 2):
        X = data[data["model"] == m1]["ROTA_conserved_accu"].values
        Y = data[data["model"] == m2]["ROTA_conserved_accu"].values
        d = X - Y
        # Test the null hypothesis that there is no accuracy difference
        stat, pval = wilcoxon(d)
        # Test the null hypothesis that the median of the differences is positive
        # i.e. m1 > m2
        g_stat, g_pval = wilcoxon(d, alternative='greater')
        # Test the null hypothesis that the median of the differences is negative
        # i.e. m2 > m1
        l_stat, l_pval = wilcoxon(d, alternative='less')
        arr.append([m1, m2, stat, pval, g_stat, g_pval, l_stat, l_pval])
conserved_wil_df = pd.DataFrame(arr, columns=["model_1", "model_2", 
                                    "two-sided_wilcoxon_stat", "two-sided_Pval",
                                   "greater_wilcoxon_stat", "greater_Pval",
                                   "less_wilcoxon_stat", "less_Pval"])
conserved_wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]] = conserved_wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]].\
apply(lambda r : ['{:.6f}'.format(x) for x in r])
conserved_wil_df
# -

#data = pd.read_csv("data/rubisco_empiricalx31x500_1-char.rotasequences.reconstruct_term_rotaseq_ALL.accuracyvREF.tsv",
#                  sep="\t", index_col=None)
# exclude MNN models
# models = [m for m in data.model.unique() if "MNN" not in m and m != "RAM55"]
models = [m for m in data.model.unique() if "RAM55" not in m]
taxa = data.taxon.unique()
data = data.sort_values(by=["taxon"])
arr = []
X = data[data["model"] == "RAM55"][["ROTA_accuracy", 
                                       "ROTA_conserved_accu",
                                      "ROTA_nonconserved_accu"]].values
X_MNN = data[data["model"] == "RAM55 MNN"][["ROTA_accuracy", 
                                       "ROTA_conserved_accu",
                                      "ROTA_nonconserved_accu"]].values
for m2 in models:
        Y = data[data["model"] == m2][["ROTA_accuracy", 
                                       "ROTA_conserved_accu",
                                      "ROTA_nonconserved_accu"]].values
        if "MNN" in m2:
            diff = X_MNN - Y
        else:
            diff = X - Y
        for d,taxon in zip(diff, taxa):
            arr.append(["RAM55", m2, taxon] + list(d))
delta_df = pd.DataFrame(arr, columns=["model_1", "model_2",
                                     "taxon", "delta_ROTA_accuracy", 
                                     "delta_ROTA_conserved_accu",
                                     "delta_ROTA_nonconserved_accu"])
delta_df = pd.merge(delta_df, data[["model", 
                                    "taxon", 
                                    "ungapped_conserved_sites"]], left_on=["model_2", 
                                                                           "taxon"], right_on=["model", 
                                                                                               "taxon"])
#delta_df = pd.merge(delta_df, data, left_on=["model_1", "taxon"], 
#                    right_on=["model", "taxon"], suffixes=('_model_2', '_RAM55'))
#delta_df["parent_model"] = delta_df["model_2"].apply(lambda x: x.split("_")[0])
delta_df.head()

delta_df.shape

print(delta_df.ungapped_conserved_sites.describe())
sb.distplot(delta_df.ungapped_conserved_sites)

# +

palette = sb.light_palette("blue", n_colors=4)[:3] + sb.light_palette("orange", n_colors=4)[:3]

models = ["rand", "rel freq", "NNC", "MNN"]

fig, ax = plt.subplots(1, figsize=(5,7))

sb.boxplot(x="model_2", y="delta_ROTA_accuracy", notch=True,
                order=models, color='white', width=.5, fliersize=0,
           linewidth=1, data=delta_df, ax=ax);
sb.stripplot(x="model_2", y="delta_ROTA_accuracy", color=".25",
             order=models, size=3, data=delta_df, ax=ax);
ax.set_ylim(bottom=0)
# iterate over boxes
for i,box in enumerate(ax.artists):
    box.set_edgecolor('black')
    box.set_facecolor('white')

    # set color of whiskers and median lines
    h_lines = [ax.lines[j] for j in range(6*i,6*(i+1))]
    for line in h_lines:
        line.set_color('black')
    # get position data for median line
    x, y  = h_lines[-2].get_xydata()[1]
    # annotate median value
    offset = ax.get_xlim()[1] / 85
    plt.text(x + offset, y, '%.1f' % y, va="center", ha='left')

ax.set_xticklabels(["Unif", "RelFreq", "NNC", "MNN"],  ha="right")
 
ax.tick_params("x", labelsize="medium", labelrotation=30, length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 prediction accuracy improvement (%)", fontsize="medium")
fig.savefig("figures/Fig_Rotaseq_homology_accu_vs_assign_rubisco.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)

# +

palette = sb.light_palette("blue", n_colors=4)[:3] + sb.light_palette("orange", n_colors=4)[:3]

models = ["NNC", "MNN"]

plot_data = delta_df.query('model in @models')
fig, ax = plt.subplots(1, figsize=(5,7))

sb.boxplot(x="model_2", y="delta_ROTA_accuracy", notch=True,
                order=models, color='white', width=.5, fliersize=0,
           linewidth=1, data=delta_df, ax=ax);
sb.stripplot(x="model_2", y="delta_ROTA_accuracy", color=".25",
             order=models, size=3, data=delta_df, ax=ax);
ax.set_ylim(bottom=0)
# iterate over boxes
for i,box in enumerate(ax.artists):
    box.set_edgecolor('black')
    box.set_facecolor('white')

    # iterate over whiskers and median lines
    for j in range(6*i,6*(i+1)):
         ax.lines[j].set_color('black')
            
ax.set_xticklabels(models,  ha="right")
 
ax.tick_params("x", labelsize="medium", labelrotation=30, length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 prediction accuracy improvement (%)", fontsize="large")
fig.savefig("figures/Fig_Rotaseq_homology_accu_vs_assign_MNN+NNC_rubisco.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)

# +
conserved_df = delta_df[["model_1", "model_2", "taxon", "delta_ROTA_conserved_accu" ]]
conserved_df.drop("model_1", axis=1, inplace=True)
conserved_df.columns = ["model", "taxon", "delta_ROTA_accuracy"]
conserved_df["AA conserved"] = [True] * len(conserved_df)

nonconserved_df = delta_df[["model_1", "model_2", "taxon", "delta_ROTA_nonconserved_accu" ]]
nonconserved_df.drop("model_1", axis=1, inplace=True)
nonconserved_df.columns = ["model", "taxon", "delta_ROTA_accuracy"]
nonconserved_df["AA conserved"] = [False] * len(nonconserved_df)
plot_data = pd.concat([conserved_df, nonconserved_df])
plot_data.head(10)
# +
fig, ax = plt.subplots(1, figsize=(6,8))
sb.boxplot(x="model", y="delta_ROTA_accuracy", 
            hue="AA conserved",notch=True, order=models, 
            fliersize=.0, data=plot_data);
sb.stripplot(x="model", y="delta_ROTA_accuracy", color=".25",
            hue="AA conserved", order=models, dodge=True,
            data=plot_data, ax=ax);
ax.axhline(y=0, color="k", linestyle="--");
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:2], labels[:2], 
          title = "AA conserved", title_fontsize = "x-small");

ax.set_xticklabels(models,  ha="right")
 
ax.tick_params("x", labelsize="medium", labelrotation=30, length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 prediction accuracy improvement (%)", fontsize="large")
fig.savefig("figures/Fig_Rotaseq_homology_accu_conservedBOTH_rubisco.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)


# +
fig, ax = plt.subplots(1, figsize=(6,8))

# Plot non-conserved sites oonly
plot_data2 = plot_data[plot_data["AA conserved"] == False]

sb.boxplot(x="model", y="delta_ROTA_accuracy", 
            notch=True, order=models, 
            fliersize=.0, data=plot_data2);
sb.stripplot(x="model", y="delta_ROTA_accuracy", color=".25", 
            order=models, dodge=True,
            data=plot_data2, ax=ax);
ax.axhline(y=0, color="k", linestyle="--");
handles, labels = ax.get_legend_handles_labels()

ax.set_xticklabels(models,  ha="right")
 
ax.tick_params("x", labelsize="medium", labelrotation=30, length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 prediction accuracy improvement (%)", fontsize="large")
fig.savefig("figures/Fig_Rotaseq_homology_accu_non-conserved_rubisco.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
