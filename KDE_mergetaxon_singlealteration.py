# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: py3.7
#     language: python
#     name: py3.7
# ---

# +
import joypy
import scipy
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sb
from numpy.random import randn
import warnings
warnings.filterwarnings('ignore')

sb.set(context='paper', style='darkgrid', rc={'figure.facecolor':'white'}, font_scale=1.2)

pd.options.display.float_format = '{:,.4f}'.format
pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
#sb.set(context='paper', style='darkgrid', rc={'figure.facecolor':'white'}, font_scale=1.2)
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
sb.set_style("ticks")
matplotlib.rc("font", serif='Helvetica Neue')
figsize = (6,7)


grey_palette = [
"#808080",  # darkest
"#8c8c8c",
"#999999",
'#a6a6a6',
"#b2b2b2",
"#bfbfbf",
"#cccccc"  # ligthest
]

# the marginal reconstrcution posterior
# based on: /nfs/research1/goldman/umberto/bioinfotree/prj/rotamer_ambiguity/dataset/3/alignmentALLxALLxALL_scaled-branches_evo_simulation.55x55_rotasequence.marginal-rec.pseudo_L.comparevREF_taxonavg
# https://bitbucket.org/uperron/rotamer_ambiguity
posterior_infile = "data/alignmentALLxALLxALL_scaled-branches_evo_simulation.55x55_rotasequence.marginal-rec.pseudo_L.comparevREF_taxonavg"

data = pd.read_csv(posterior_infile, sep="\t", header=0, index_col=None)

# +
# produces supplementary figure 6
# labelled fig:correct_state_posterior_KDE
curr_taxanum, curr_align_len, curr_replicates = [32, 20, 1000]
outfile = "figures/Fig_correct_state_posterior_KDE.pdf"
outfile_svg = "figures/Fig_correct_state_posterior_KDE.svg"
plot_data = data.query("taxanum == @curr_taxanum &\
align_len == @curr_align_len &\
scaling_factor in [1]")
S = plot_data["alteration"].astype(str) + "_" + plot_data["masking_factor"].astype(str)
S.index = plot_data.index
plot_data["alt_f"] = S
alt_f = sorted(plot_data["alt_f"].unique())

Dm = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "masked"})
Dd = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "discarded"})
plot_data = plot_data.append(Dm)
plot_data = plot_data.append(Dd)
plot_data = plot_data[plot_data["alteration"] != "No_alteration"]
plot_data = plot_data[plot_data["alt_f"] != "discarded_1.0"]
plot_data = plot_data[plot_data["alt_f"] != "masked_1.0"]
di = {"_0.0" : "", "_" : " ","masked" : "Masking factor","discarded" : "Discarding factor"}
plot_data["alt_f"] = plot_data["alt_f"].replace(di, regex=True)

#custom sorting list
sorter = ['Discarding factor 0.875',
          'Discarding factor 0.75',
          'Discarding factor 0.5',
          'Discarding factor 0.25',
          'Discarding factor 0.125',
          'No alteration',
          'Masking factor 0.125',
          'Masking factor 0.25',
          'Masking factor 0.5',
          'Masking factor 0.75',
          'Masking factor 0.875']
# Create a dictionary that defines the order for sorting
sorterIndex = dict(zip(sorter,range(len(sorter))))

# Generate a rank column that will be used to sort
# the dataframe numerically
plot_data['alt_rank'] = plot_data['alt_f'].map(sorterIndex)
plot_data = plot_data.sort_values("alt_rank", ascending=True)
plot_data['alt_rank'] = plot_data['alt_rank'].apply(str)

sb.set_style("ticks")

rArtist = plt.scatter(x=0, y=0, color='r', marker='v')
mArtist = plt.scatter(x=0, y=0, color='m', marker='v')
oArtist = plt.scatter(x=0, y=0, color='orange', marker='v')

fig, axes = joypy.joyplot(plot_data, 
                          by="alt_rank", 
                          column="ref_ROTA_state_pseudo_L", 
                          labels=sorter,
                          x_range=[0,1], 
                          #range_style='own',
                          #kind="normalized_counts", 
                          kind="kde", 
                          legend=False, 
                          figsize=(5,7), 
                          overlap=2, 
                          linewidth=.5, 
                          bins=30,  
                          grid="y", 
                          ylim="own",
                          fade=True)
ticks = [0, .2, .4, .6, .8, 1]

#select first ax
ax = axes[0]
#Create custom artists
#Create legend 
ax.legend([rArtist, 
           mArtist, 
           oArtist],
          ['Median discarded', 
           'Median no alteration', 
           'Median masked'])

for ax_index, ax in enumerate(axes):
    ax.tick_params(axis='both', which='major')
    ax.set_yticklabels(ax.get_yticklabels(), fontsize="xx-small")
    ax.set_xticks(ticks)
    ax.set_xticklabels(ticks, fontsize="xx-small")
    median = plot_data[plot_data["alt_rank"] == str(ax_index)].ref_ROTA_state_pseudo_L.median()
    ax.set_ylim(0, 5.5)
    ymax = ax.get_ylim()[1]
    if ax_index == 5:
        ax.scatter(x=median, y=ymax, color='m', marker='v', zorder=10)
    elif ax_index < 5:
        ax.scatter(x=median, y=ymax, color='r', marker='v', zorder=10)
    else:
        ax.scatter(x=median, y=ymax, color='orange', marker='v', zorder=10)
    if ax_index == 11:
        ax.tick_params("x", labelsize="small", length=3)
        ax.set_ylabel("Correct state posterior KDE", fontsize="small", labelpad=15)
        ax.set_xlabel("Correct state posterior", fontsize="small", labelpad=10)
        ax.axhline(y=ax.get_ylim()[0], linewidth=1.5, color='k')

fig.set_size_inches(figsize)
fig.savefig(outfile, format='pdf', bbox_inches='tight');
fig.savefig(outfile_svg, format='svg', bbox_inches='tight');
# -

grey_palette = [
"#808080", # darkest
"#8c8c8c",
"#999999",
'#a6a6a6',
"#b2b2b2",
"#bfbfbf",
"#cccccc" # ligthest
] 
sb.palplot(grey_palette)

# +
# produces supplementary figure 6 IN GREYSCALE
# labelled fig:correct_state_posterior_KDE

curr_taxanum, curr_align_len, curr_replicates = [32, 20, 1000]
outfile = "figures/Fig_correct_state_posterior_KDE_greyscale.pdf"
outfile_svg = "figures/Fig_correct_state_posterior_KDE_greyscale.svg"
plot_data = data.query("taxanum == @curr_taxanum &\
align_len == @curr_align_len &\
scaling_factor in [1]")
S = plot_data["alteration"].astype(
    str) + "_" + plot_data["masking_factor"].astype(str)
S.index = plot_data.index
plot_data["alt_f"] = S
alt_f = sorted(plot_data["alt_f"].unique())

Dm = plot_data[plot_data["alteration"] == "No_alteration"].replace(
    {"No_alteration": "masked"})
Dd = plot_data[plot_data["alteration"] == "No_alteration"].replace(
    {"No_alteration": "discarded"})
plot_data = plot_data.append(Dm)
plot_data = plot_data.append(Dd)
plot_data = plot_data[plot_data["alteration"] != "No_alteration"]
plot_data = plot_data[plot_data["alt_f"] != "discarded_1.0"]
plot_data = plot_data[plot_data["alt_f"] != "masked_1.0"]
di = {"_0.0": "", "_": " ", "masked": "Masking factor",
    "discarded": "Discarding factor"}
plot_data["alt_f"] = plot_data["alt_f"].replace(di, regex=True)

# custom sorting list
sorter = ['Discarding factor 0.875',
          'Discarding factor 0.75',
          'Discarding factor 0.5',
          'Discarding factor 0.25',
          'Discarding factor 0.125',
          'No alteration',
          'Masking factor 0.125',
          'Masking factor 0.25',
          'Masking factor 0.5',
          'Masking factor 0.75',
          'Masking factor 0.875']
# Create a dictionary that defines the order for sorting
sorterIndex = dict(zip(sorter, range(len(sorter))))

# Generate a rank column that will be used to sort
# the dataframe numerically
plot_data['alt_rank'] = plot_data['alt_f'].map(sorterIndex)
plot_data = plot_data.sort_values("alt_rank", ascending=True)
plot_data['alt_rank'] = plot_data['alt_rank'].apply(str)

sb.set_style("ticks")


fig, axes=joypy.joyplot(plot_data,
                          by="alt_rank",
                          column="ref_ROTA_state_pseudo_L",
                          labels=sorter,
                          x_range=[0, 1],
                          # range_style='own',
                          # kind="normalized_counts",
                          color=grey_palette[-1],
                          kind="kde",
                          legend=False,
                          figsize=(5, 7),
                          overlap=2,
                          linewidth=1,
                          bins=30,
                          grid="y",
                          ylim="own",
                          fade=True)
ticks=[0, .2, .4, .6, .8, 1]

# select first ax
ax=axes[0]
# Create custom artists
kArtist = plt.scatter(x=0, y=0, color='k', marker='v',
                      linewidths=.5, edgecolor='k')
gArtist = plt.scatter(x=0, y=0, color=grey_palette[0], marker='v', 
                      linewidths=.5, edgecolor='k')
wArtist=plt.scatter(x = 0, y = 0, color = 'w', marker = 'v', 
                    linewidths = .5, edgecolor = 'k')
# Create legend
ax.legend([gArtist,
           kArtist,
           wArtist],
          ['Median discarded',
           'Median no alteration',
           'Median masked'])

for ax_index, ax in enumerate(axes):
    ax.tick_params(axis = 'both', which = 'major')
    ax.set_yticklabels(ax.get_yticklabels(), fontsize = "xx-small")
    ax.set_xticks(ticks)
    ax.set_xticklabels(ticks, fontsize = "xx-small")
    #median=plot_data[plot_data["alt_rank"] == str(
    #    ax_index)].ref_ROTA_state_pseudo_L.median()
    #ax.set_ylim(0, 5.5)
    #ymax=ax.get_ylim()[1]
    
    x,y = [0,0]
    if ax_index != 11:
        # Extract the data of the kde line
        x,y = ax.get_lines()[1].get_data()
        # Integrate to calculate the cumulative distribution function (CDF) 
        # initial is 0 so the result has same length as x
        cdf = scipy.integrate.cumtrapz(y, x, initial=0)
        # Find the value that makes CDF equal .5 i.e. median
        nearest_05 = np.abs(cdf-0.5).argmin()
        x_median = x[nearest_05]
        y_median = y[nearest_05]
        
    offset = ax.get_ylim()[1] / 30
    y_median += offset
    # place markers on median
    if ax_index == 5:
        ax.scatter(x = x_median, y =y_median, color='k', marker='v', 
                   zorder=10, linewidths=.5, edgecolor='k')
    elif ax_index < 5:
        ax.scatter(x=x_median, y=y_median, color=grey_palette[0], 
                   marker='v', zorder=10, linewidths=.5, edgecolor='k')
    else:
        ax.scatter(x=x_median, y=y_median, color="w", marker='v', 
                   zorder=12, linewidths=.5, edgecolor='k')
    if ax_index == 11:
        ax.tick_params("x", labelsize="small", length=3)
        ax.set_ylabel("Correct state posterior KDE",
                      fontsize="small", labelpad=15)
        ax.set_xlabel("Correct state posterior", fontsize="small", labelpad=10)
        ax.axhline(y=ax.get_ylim()[0], linewidth=1.5, color='k')

fig.set_size_inches(figsize)
fig.savefig(outfile, format='pdf', bbox_inches='tight');
fig.savefig(outfile_svg, format='svg', bbox_inches='tight');
# -

ax = sb.kdeplot(plot_data[plot_data["alt_rank"] == "0"].ref_ROTA_state_pseudo_L)
ax.set_ylim(0,5.5)

ax = sb.kdeplot(plot_data[plot_data["alt_rank"] == "3"].ref_ROTA_state_pseudo_L)
ax.set_ylim(0,5.5)
