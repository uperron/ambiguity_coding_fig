# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import ticker
import seaborn as sb
import warnings
warnings.filterwarnings('ignore')
from scipy.stats import wilcoxon
from itertools import combinations
import warnings
warnings.filterwarnings('ignore')

pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

palette = [sb.light_palette("blue", n_colors=3)[1]] +\
sb.light_palette("green", n_colors=3) +\
sb.light_palette("red", n_colors=3)

# homolog rotasequence reconstruction using RAM55 and joint vs LG or RUM20 + rotastate assignment methods

# NNC = nearest neighbour configuration
# the AA sequence is reconstructed using joint and the 20x20 models, then the rotamer configuration
# is assigned based on the nearest rotasequence on the tree. Only sites where the AA is conserved in 
# the reconstruction contribute to the accuracy.

# MMC = masked nearest neighbour
# same as above only the nearest rotasequence is masked and the thus the next one over 
# is used to assign the rotamer configuration

# based on uperron@ebi:/nfs/research1/goldman/umberto/bioinfotree/prj/rotamer_ambiguity/dataset/\
# rubisco_empirical/rubisco_empiricalx31x500_1-char.rotasequences.reconstruct_term_rotaseq_ALL.accuracyvREF.tsv
# https://bitbucket.org/uperron/rotamer_ambiguity
accu_infile = "data/rubisco_empiricalx31x500_1-char.rotasequences.reconstruct_term_rotaseq_ALL.accuracyvREF.tsv"
data = pd.read_csv(accu_infile, sep="\t", index_col=None)
models = data.model.unique()
data = data.sort_values(by=["taxon"])
data.head()

# +
# The Wilcoxon signed-rank test tests the null hypothesis that two related paired samples come from 
# the same distribution. In particular, it tests whether the distribution of the differences x - y 
# is symmetric about zero. It is a non-parametric version of the paired T-test. A typical rule is 
# to require that n > 20 (here n=31).

arr = []
for m1, m2 in combinations(models, 2):
        X = data[data["model"] == m1]["ROTA_accuracy"].values
        Y = data[data["model"] == m2]["ROTA_accuracy"].values
        d = X - Y
        # Test the null hypothesis that there is no accuracy difference
        stat, pval = wilcoxon(d)
        # Test the null hypothesis that the median of the differences is positive
        # i.e. m1 > m2
        g_stat, g_pval = wilcoxon(d, alternative='greater')
        # Test the null hypothesis that the median of the differences is negative
        # i.e. m2 > m1
        l_stat, l_pval = wilcoxon(d, alternative='less')
        arr.append([m1, m2, stat, pval, g_stat, g_pval, l_stat, l_pval])
wil_df = pd.DataFrame(arr, columns=["model_1", "model_2", 
                                    "two-sided_wilcoxon_stat", "two-sided_Pval",
                                   "greater_wilcoxon_stat", "greater_Pval",
                                   "less_wilcoxon_stat", "less_Pval"])
wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]] = wil_df[["two-sided_Pval", "greater_Pval", "less_Pval"]].\
apply(lambda r : ['{:.6f}'.format(x) for x in r])
wil_df
# -

data = pd.read_csv("data/rubisco_empiricalx31x500_1-char.rotasequences.reconstruct_term_rotaseq_ALL.accuracyvREF.tsv",
                  sep="\t", index_col=None)
# exclude MNN models
models = [m for m in data.model.unique() if "MNN" not in m]
taxa = data.taxon.unique()
data = data.sort_values(by=["taxon"])
arr = []
X = data[data["model"] == "RAM55"]["ROTA_accuracy"].values
for m2 in models[1:]:
        Y = data[data["model"] == m2]["ROTA_accuracy"].values
        diff = X - Y
        for d,taxon in zip(diff, taxa):
            arr.append(["RAM55", m2, taxon, d])
delta_df = pd.DataFrame(arr, columns=["model_1", "model_2",
                                     "taxon", "delta_ROTA_accuracy"])
delta_df = pd.merge(delta_df, data, left_on=["model_2", "taxon"], right_on=["model", "taxon"])
delta_df = pd.merge(delta_df, data, left_on=["model_1", "taxon"], 
                    right_on=["model", "taxon"], suffixes=('_model_2', '_RAM55'))
delta_df["parent_model"] = delta_df["model_2"].apply(lambda x: x.split("_")[0])
delta_df.head(10)

# +
# this produces Supplementary Figure 4
# labelled fig:Rotaseq_rec_accu_vs_assign_rubisco
palette = sb.light_palette("blue", n_colors=4)[:3] + sb.light_palette("orange", n_colors=4)[:3]

fig, ax = plt.subplots(1, figsize=(6,8))

sb.boxplot(x="model_2", y="delta_ROTA_accuracy", notch=True,
                fliersize=.01, palette=palette, data=delta_df, ax=ax);
sb.swarmplot(x="model_2", y="delta_ROTA_accuracy", color=".25",
             data=delta_df, ax=ax);
ax.axhline(y=0, color="k", linestyle="--");

model_labels = [m.replace("_", " ") for m in models[1:]]
ax.set_xticklabels(model_labels,  ha="right")
 
ax.tick_params("x", labelsize="medium", labelrotation=30, length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 reconstruction accuracy improvement (%)", fontsize="large")
fig.savefig("figures/Fig_Rotaseq_rec_accu_vs_assign_rubisco.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)

# +
# this produces Figure 8
# labelled fig:empirical_MNN_accu_impovement

palette = sb.light_palette("blue", n_colors=4)[:3] + sb.light_palette("orange", n_colors=4)[:3]
palette = palette[2], palette[5]
fig, ax = plt.subplots(1, figsize=(4,8))

data = pd.read_csv("data/rubisco_empiricalx31x500_1-char.rotasequences.reconstruct_term_rotaseq_ALL.accuracyvREF.tsv",
                  sep="\t", index_col=None)
#only MNN models
taxa = data.taxon.unique()
data = data.sort_values(by=["taxon"])
arr = []
models =  ['LG_MNN', 'RUM20_MNN']
X = data[data["model"] == "RAM55MNN"]["ROTA_accuracy"].values

# RAM55's improvement in accuracy over a 20-state model + MNN
for m2 in models:
        Y = data[data["model"] == m2]["ROTA_accuracy"].values
        diff = X - Y
        for d,taxon in zip(diff, taxa):
            arr.append(["RAM55MNN", m2, taxon, d])         
plot_delta_df = pd.DataFrame(arr, columns=["model_1", "model_2",
                                     "taxon", "delta_ROTA_accuracy"])

sb.boxplot(x="model_2", y="delta_ROTA_accuracy", notch=True,
                fliersize=.01, data=plot_delta_df, ax=ax);
sb.swarmplot(x="model_2", y="delta_ROTA_accuracy", color=".25",
             data=plot_delta_df, ax=ax);
ax.axhline(y=0, color="k", linestyle="--");
ax.set_xticklabels(['LG MNN', 'RUM20 MNN'],  ha="center")

ax.tick_params("x", labelsize="medium", length=3)
ax.tick_params("y", labelsize="medium", length=3)
ax.set_xlabel("", fontsize="large")
ax.set_ylabel("RAM55 reconstruction accuracy improvement (%)", fontsize="large")
fig.savefig("figures/Fig_empirical_MNN_accu_impovement.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
plt.show()
# -


