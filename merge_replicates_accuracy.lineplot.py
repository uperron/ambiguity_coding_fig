# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.patches import Patch
import seaborn as sb
import warnings
warnings.filterwarnings('ignore')

pd.options.display.max_seq_items = 2000
# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Set default font size
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')
figsize = (7,3)

# based on:
# /nfs/research1/goldman/umberto/bioinfotree/prj/rotamer_ambiguity/dataset/4/alignmentALLxALLxALL_scaled-branches_evo_simulation.20x20andLG_rotasequence.joint-rec_assign_rota.comparevREF
# https://bitbucket.org/uperron/rotamer_ambiguity

# simulation is performed along a 32-taxa random tree, simulated alignments
# are 20-sites long, 1000 replicates are generated using RAM55 
curr_taxanum, curr_align_len, curr_replicates = [32, 20, 1000]

# ancestral rotamer sequence reconstruction accuracy 
# using simulated mixed data, RAM55 and joint reconstruction
RAM55_rec_accu_infile = "data/alignmentALLxALLxALL_scaled-branches_evo_simulation.55x55_rotasequence.joint-rec.comparevREF"

# reconstruction accuracy using RUM20 and LG plus
# rotamer state assignment
RUM_LG_infile = "data/alignmentALLxALLxALL_scaled-branches_evo_simulation.20x20andLG_rotasequence.joint-rec_assign_rota.comparevREF"

data = pd.read_csv(RAM55_rec_accu_infile,
                   sep="\t", header=0, index_col=None)
data = data.replace({np.nan : "No_alteration"})

data2 = pd.read_csv(RUM_LG_infile,
                    sep="\t", header=0, index_col=None)


# +
# this produces figure 6 aka Fig_Rotaseq_rec_accu_mix.pdf, 
# labelled as fig:Rotaseq_rec_accu_mix

#outfile = "Sim-Align_MixMask_" + rec_method + "-rec_RUM55_" + str(curr_taxanum) +\
#"x20x1000.ROTA_accuracy.compare_vREF.scalegroup.merge_replicates.merge_taxa.\
#lineplot.mask-only.pdf"
outfile = "figures/Fig_Rotaseq_rec_accu_mix.pdf"

plot_data = data.query("taxanum == @curr_taxanum & \
align_len == @curr_align_len & scaling_factor in [.1, 1]")
plot_data.loc[:, "model"] = ["RAM55"]*len(plot_data)

S = plot_data["alteration"].astype(str) + "_" + plot_data["masking_factor"].astype(str)
S.index = plot_data.index
plot_data["alt_f"] = S
plot_data["f"] = [1 - m for m in plot_data.masking_factor.tolist()]


Dm = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "masked"})
Dd = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "discarded"})
plot_data = plot_data.append(Dm)
plot_data = plot_data.append(Dd)
plot_data = plot_data[plot_data["alteration"] != "No_alteration"]
plot_data = plot_data[plot_data["alteration"] == "masked"]
X = plot_data["f"].unique().tolist()
# one row per scaling factor
g = sb.FacetGrid(col="scaling_factor", data=plot_data, )
g.map(sb.lineplot, "f", "ROTA_accuracy", 
      markers=True, err_style="bars", ci="sd")
g.add_legend()

for ax in g.axes[:,0]:
    ax.set_ylabel("Reconstruction accuracy (%)")

for ax_row in g.axes:
    for ax, scaling_factor, title in zip(ax_row, 
                                         plot_data.scaling_factor.unique().tolist(), ["A)", "B)"]):
        y = plot_data.query('alt_f == "No_alteration_0.0" &\
        scaling_factor == @scaling_factor')["ROTA_accuracy"].mean()
        y2 = data2.query("taxanum == @curr_taxanum & \
align_len == @curr_align_len & \
model == '20x20+rand' & scaling_factor == @scaling_factor")["ROTA_accuracy"].mean()
        ax.axhline(y, linestyle='-.', color="k")
        ax.axhline(y2, linestyle='--', color="r")
        ax.tick_params(axis='both', which='major',length=3, pad=.5)
        ax.set_yticklabels(ax.get_yticks())
        ax.set_xticks(X) 
        ax.set_xticklabels(X)
        ax.set_xlabel("Masking factor")
        ax.set_title(title, loc="left") 
        ax.set_title("Scaling factor = %.1f" % (scaling_factor), loc="center")       
fig = g.fig
fig.set_size_inches(figsize)
fig.savefig(outfile, format='pdf', bbox_inches='tight')

# +
# produces figure 7 aka Fig_Rotaseq_rec_accu_mix.pdf, 
# labelled as fig:Rotaseq_info_gain_mix

#outfile = "Sim-Align_MixMask_" + rec_method + "-rec_RUM55_" + str(curr_taxanum) \
# #+ "x20x1000.ROTA_accuracy.compare_vREF.scalegroup." + \
#"merge_replicates.merge_taxa.lineplot.pdf"
outfile = "figures/Fig_Rotaseq_info_gain_mix.pdf"

plot_data = data.query("taxanum == @curr_taxanum & align_len == @curr_align_len & scaling_factor in [.1, 1]")
plot_data.loc[:, "model"] = ["RAM55"]*len(plot_data)

S = plot_data["alteration"].astype(str) + "_" + plot_data["masking_factor"].astype(str)
S.index = plot_data.index
plot_data["alt_f"] = S

Dm = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "masked"})
Dd = plot_data[plot_data["alteration"] == "No_alteration"].replace({"No_alteration" : "discarded"})
plot_data = plot_data.append(Dm)
plot_data = plot_data.append(Dd)
plot_data = plot_data[plot_data["alteration"] != "No_alteration"]

plot_data["f"] = [1 - m for m in plot_data.masking_factor.tolist()]
X = plot_data["f"].unique().tolist()

# one row per scaling factor
g = sb.relplot(x="f", y="ROTA_accuracy",
    hue="alteration", style="alteration", col="scaling_factor",
    kind="line", ci=None, markers=True, dashes=False, data=plot_data)
g.add_legend()

for ax in g.axes[:,0]:
    ax.set_ylabel("Reconstruction accuracy (%)")

for ax_row in g.axes:
    for ax, scaling_factor, title in zip(ax_row,
                                         plot_data.scaling_factor.unique().tolist(), ["A)", "B)"]):
        y = plot_data.query('alt_f == "masked_0.5" & scaling_factor == @scaling_factor')["ROTA_accuracy"].mean()
        ax.plot((.5,.875), (y,y), linestyle=':', color='k')
        ax.tick_params(axis='both', which='major', length=3, pad=.5)
        ax.set_yticklabels(ax.get_yticks())
        ax.set_xticks(X) 
        ax.set_xticklabels(X)
        ax.set_xlabel("Masking factor")
        ax.set_title("Scaling factor = %.1f" % (scaling_factor), loc="center") 
        ax.set_title(title, loc="left")  
fig = g.fig
fig.set_size_inches(figsize)
fig.savefig(outfile, format='pdf', bbox_inches='tight')
